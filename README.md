# Recuperatorio Mod 6

## Instalar dependencias del Projecto

```
npm install
```

### Configuración variables de Entorno

Copiar el archivo `.env.example` con el nombre `.env` en el mismo directorio raiz.

### Iniciar Json Server

Abrir consola y ubicar el path en el directorio /db

```
json-server db.json --watch --port=3000
```

### Iniciar el Proyecto

```
npm run serve
```
